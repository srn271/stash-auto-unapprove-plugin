package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.AuthorisationException;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.transaction.SimpleSalTransactionTemplate;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettingsChangedEvent;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.DummySecurityService;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static com.atlassian.bitbucket.internal.unapprove.DefaultAutoUnapproveService.PLUGIN_KEY;
import static com.atlassian.bitbucket.internal.unapprove.DefaultAutoUnapproveService.keyFor;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAutoUnapproveServiceTest {

    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private PullRequestService pullRequestService;
    @Mock
    private Repository repository;
    @Spy
    private SecurityService securityService = new DummySecurityService();
    private DefaultAutoUnapproveService service;
    @Mock
    private PluginSettings settings;
    @Mock
    private PluginSettingsFactory settingsFactory;
    @Spy
    private TransactionTemplate transactionTemplate = new SimpleSalTransactionTemplate();
    @Mock
    private PermissionValidationService validationService;

    @Before
    public void setup() {
        when(repository.getId()).thenReturn(42);
        when(settingsFactory.createSettingsForKey(eq(PLUGIN_KEY))).thenReturn(settings);

        service = new DefaultAutoUnapproveService(eventPublisher, settingsFactory, pullRequestService,
                securityService, transactionTemplate, validationService);
    }

    @Test
    public void testCleanup() {
        service.cleanup(repository);

        verify(settings).remove(eq(keyFor(repository)));
    }

    @Test
    public void testIsEnabledForPullRequest() {
        PullRequest pullRequest = mockPullRequest();

        when(settings.get(eq(keyFor(repository)))).thenReturn("1");

        assertTrue(service.isEnabled(pullRequest));

        verify(settings).get(eq(keyFor(repository)));
    }

    @Test
    public void testIsEnabledForRepository() {
        assertFalse(service.isEnabled(repository));

        verify(settings).get(eq(keyFor(repository)));
    }

    @Test
    public void testSetEnabledToDisable() {
        when(settings.remove(eq(keyFor(repository)))).thenReturn("1");

        service.setEnabled(repository, false);

        ArgumentCaptor<AutoUnapproveSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).remove(eq(keyFor(repository)));
        verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));

        AutoUnapproveSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(repository, event.getRepository());
        assertSame(service, event.getSource());
        assertFalse(event.isEnabled());
    }

    @Test
    public void testSetEnabledToEnable() {
        service.setEnabled(repository, true);

        ArgumentCaptor<AutoUnapproveSettingsChangedEvent> eventCaptor =
                ArgumentCaptor.forClass(AutoUnapproveSettingsChangedEvent.class);

        verify(eventPublisher).publish(eventCaptor.capture());
        verify(settings).put(eq(keyFor(repository)), eq("1"));
        verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));

        AutoUnapproveSettingsChangedEvent event = eventCaptor.getValue();
        assertNotNull(event);
        assertSame(repository, event.getRepository());
        assertSame(service, event.getSource());
        assertTrue(event.isEnabled());
    }

    @Test(expected = AuthorisationException.class)
    public void testSetEnabledWithoutPermission() {
        doThrow(AuthorisationException.class)
                .when(validationService)
                .validateForRepository(same(repository), same(Permission.REPO_ADMIN));

        try {
            service.setEnabled(repository, true);
        } finally {
            verify(validationService).validateForRepository(same(repository), same(Permission.REPO_ADMIN));
            verifyZeroInteractions(eventPublisher, settings);
        }
    }

    @Test
    public void testWithdrawApprovals() {
        PullRequestParticipant first = mockParticipant();
        PullRequestParticipant second = mockParticipant();

        PullRequest pullRequest = mockPullRequest();
        when(pullRequest.getReviewers()).thenReturn(ImmutableSet.of(first, second));

        service.withdrawApprovals(pullRequest);

        InOrder ordered = inOrder(pullRequestService, securityService, transactionTemplate);
        ordered.verify(transactionTemplate).execute(any());
        ordered.verify(securityService).impersonating(same(first.getUser()), notNull());
        ordered.verify(pullRequestService).setReviewerStatus(eq(42), eq(1L), same(PullRequestParticipantStatus.UNAPPROVED));
        ordered.verify(securityService).impersonating(same(second.getUser()), notNull());
        ordered.verify(pullRequestService).setReviewerStatus(eq(42), eq(1L), same(PullRequestParticipantStatus.UNAPPROVED));

        verify(pullRequest, never()).getParticipants(); // Participants should not be reset; they can't approve
        verify(pullRequest).getReviewers();             // Reviewers should always be reset
    }

    @Test
    public void testWithdrawApprovalsHandlesIllegalPullRequestState() {
        doThrow(IllegalPullRequestStateException.class)
                .when(pullRequestService)
                .setReviewerStatus(anyInt(), anyLong(), any());

        PullRequestParticipant participant = mockParticipant();

        PullRequest pullRequest = mockPullRequest();
        when(pullRequest.getReviewers()).thenReturn(Collections.singleton(participant));

        service.withdrawApprovals(pullRequest);

        verify(pullRequest, never()).getParticipants();
        verify(pullRequest).getReviewers();
        verify(pullRequestService).setReviewerStatus(eq(42), eq(1L), same(PullRequestParticipantStatus.UNAPPROVED));
    }

    private static PullRequestParticipant mockParticipant() {
        ApplicationUser user = mock(ApplicationUser.class);

        PullRequestParticipant participant = mock(PullRequestParticipant.class);
        when(participant.getUser()).thenReturn(user);

        return participant;
    }

    private PullRequest mockPullRequest() {
        PullRequestRef toRef = mock(PullRequestRef.class);
        when(toRef.getRepository()).thenReturn(repository);

        PullRequest pullRequest = mock(PullRequest.class);
        when(pullRequest.getId()).thenReturn(1L);
        when(pullRequest.getToRef()).thenReturn(toRef);

        return pullRequest;
    }
}
